//
//  ViewController.swift
//  Weather App
//
//  Created by Anna Stadnikovaon 08.01.20.
//  Copyright © 2020 Anna Stadnikova. All rights reserved.
//

import UIKit
import CoreLocation


class ViewController: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate{
 
    
    @IBOutlet weak var currentWeatherIcon: UIImageView!
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var windNowLabel: UILabel!
    @IBOutlet weak var temperatureNowLabel: UILabel!
    @IBOutlet weak var summaryNowLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!

    @IBOutlet weak var searchBar: UISearchBar!
    
    var forecastNowData = [WeatherNow]()
    var forecastDailyData = [WeatherDaily]()
    override func viewDidLoad() {
        super.viewDidLoad()
         assignbackground()
        if(self.forecastDailyData.isEmpty && self.forecastNowData.isEmpty){
          
              self.cityLabel.text = "Dortmund"
            
              updateWeatherForNewLocation(locationString: "Dortmund")
        }
     

        if(!self.forecastNowData.isEmpty){
            setCurrentWeather(currentWeather: self.forecastNowData)
        }

        if(!self.forecastDailyData.isEmpty){
            print(self.tableView)
        self.tableView.reloadData()
        }
        self.tableView.dataSource = self
        self.tableView.delegate = self

        searchBar.delegate = self
      

   }
    func setCurrentWeather(currentWeather: [WeatherNow]){
        self.summaryNowLabel.text = currentWeather[0].summary
        self.temperatureNowLabel.text = String(Int(currentWeather[0].temperature)) + " °C"
        self.windNowLabel.text = "wind speed: " + String(currentWeather[0].wind) + " km/h"
        self.currentWeatherIcon.image = UIImage(named: currentWeather[0].icon)
    }
    func assignbackground(){
        
        let background = UIImage(named: checkDayTime())
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
        
    }
    
    func checkDayTime() -> String{
        //http response from the api returns only local date, so no need to use it from there
        let hour = Calendar.current.component(.hour, from: Date())
        
        if(hour>8 && hour<20){
            return "day"
        }
        else {
            return "night"
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if let locationString = searchBar.text, !locationString.isEmpty{
            self.cityLabel.text = locationString
        
            updateWeatherForNewLocation(locationString: locationString)
        }
    }
    

    func setNumberOfSection(tableView: UITableView)-> Int{
        return self.forecastDailyData.count
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

        if(!self.forecastDailyData.isEmpty){
  
          return formatDate(date: self.forecastDailyData[section].date)
        }
        else{
            return ""
        }
        
    }
    
     func tableView(_ tableView: UITableView,  cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dailyForecast", for: indexPath)
      
        if(!self.forecastDailyData.isEmpty){
            print(self.forecastDailyData)
            let forecast = self.forecastDailyData[indexPath.section]
            
            cell.textLabel?.text = forecast.summary
            cell.detailTextLabel?.text = "\(Int(forecast.temperature)) °C"
            cell.imageView?.image = UIImage(named: forecast.icon)
        }
        else{
            cell.textLabel?.text = "Loading"
        }
        

        return cell
        
    }
    
    
    func updateWeatherForNewLocation(locationString: String){
        CLGeocoder().geocodeAddressString(locationString){
            (placemarks:[CLPlacemark]?, error: Error?) in
            if error == nil {
                if let location = placemarks?.first?.location {
                    WeatherNow.forecastNow(withLocation: location.coordinate, completion: {(results:[WeatherNow]?) in
                        if let weatherNow = results {
                           
                            self.forecastNowData = weatherNow

                            DispatchQueue.main.async{
                            
                                self.viewDidLoad()
                
                            }
                        
                        
                        }
                })
                    WeatherDaily.forecastDaily(withLocation: location.coordinate, completion: {(results:[WeatherDaily]?) in
                        if let weatherDaily = results {
                            self.forecastDailyData = weatherDaily
                          
                                                DispatchQueue.main.async{
                                                    print(self.forecastDailyData.count)
                                            
                                                  self.viewDidLoad()
                                                }
                       
                        }
                    })

            }
        }
    }
    }
    
    

    func formatDate(date: Int) -> String{
        let date = Date(timeIntervalSince1970: TimeInterval(date))
        
        let template = "EEEEdMMM"
        
        let format = DateFormatter.dateFormat(fromTemplate: template, options: 0, locale: NSLocale.current)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        dateFormatter.timeZone = .current
        let localDate = dateFormatter.string(from: date)
        return localDate
    }


}

