//
//  WeatherApi.swift
//  Weather App
//
//  Created by Anna Stadnikova on 08.01.20.
//  Copyright © 2020 Anna Stadnikova. All rights reserved.
//

import Foundation
import CoreLocation

struct WeatherNow {
    let summary: String
    let icon: String
    let temperature: Double
    let wind: Double
    

    
    enum SerializationError: Error {
        case missing(String)
        case invalid(String, Any)
    }
    init(json: [String:Any]) throws {
   
        guard let summary = json["summary"] as? String else {throw SerializationError.missing("summary is missing")}
        guard let icon = json ["icon"] as? String else {throw SerializationError.missing("icon is missing")}
        guard let temperature = json["temperature"] as? Double else {throw SerializationError.missing("wind is missing")}
        guard let wind = json["windSpeed"] as? Double else {throw SerializationError.missing("wind is missing")}

        
        self.summary=summary
        self.icon = icon
        self.temperature = temperature
        self.wind=wind
        
    }
    
    static let basePath = "https://api.darksky.net/forecast/f88b3c8edd6a4f46295b4ac26c00a880/"
    static let pathOptions = "?units=si"
    
    
    static func forecastNow(withLocation location: CLLocationCoordinate2D, completion: @escaping ([WeatherNow]) ->())
    {
        let url = basePath + "\(location.latitude),\(location.longitude)" + pathOptions
        let request = URLRequest(url: URL(string: url)!)

        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error:Error?) in

            var forecastArray:[WeatherNow] = []

            if let data = data {
                do {
                   
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]{
                        
                        if let currentData = json["currently"] as? [String: Any]{
                           
    
                                if let weatherObject = try? WeatherNow(json:currentData){
                                
                                        forecastArray.append(weatherObject)

                            }
                        }
                    }
                } catch {
                    print(error.localizedDescription)
                }
                completion(forecastArray)
            }

        }
        task.resume()

    }
    
}
struct WeatherDaily {
    let summary: String
    let icon: String
    let temperature: Double
    let wind: Double
    let date: Int //unix time format
    
    
    enum SerializationError: Error {
        case missing(String)
        case invalid(String, Any)
    }
    init(json: [String:Any]) throws {
        
        guard let summary = json["summary"] as? String else {throw SerializationError.missing("summary is missing")}
        guard let icon = json ["icon"] as? String else {throw SerializationError.missing("icon is missing")}
        guard let temperature = json["temperatureHigh"] as? Double else {throw SerializationError.missing("wind is missing")}
        guard let wind = json["windSpeed"] as? Double else {throw SerializationError.missing("wind is missing")}
           guard let date = json["time"] as? Int else {throw SerializationError.missing("time is missing")}
        
        self.summary=summary
        self.icon = icon
        self.temperature = temperature
        self.wind=wind
        self.date=date
        
    }
    
    static let basePath = "https://api.darksky.net/forecast/f88b3c8edd6a4f46295b4ac26c00a880/"
    static let pathOptions = "?units=si"
    
    static func forecastDaily(withLocation location: CLLocationCoordinate2D, completion: @escaping ([WeatherDaily]) ->())
    {
        let url = basePath + "\(location.latitude),\(location.longitude)" + pathOptions
        let request = URLRequest(url: URL(string: url)!)
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error:Error?) in
            
            var forecastArray:[WeatherDaily] = []
            
            if let data = data {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]{
                        if let dailyForecast = json["daily"] as? [String:Any]{
                            if let dailyData = dailyForecast["data"] as? [[String:Any]] {
                                for dataPoint in dailyData{
                                    if let weatherObject = try? WeatherDaily(json:dataPoint){
                                        forecastArray.append(weatherObject)
                                    }
                                }
                            }
                        }
                    }
                } catch {
                    print(error.localizedDescription)
                }
                completion(forecastArray)
            }
            
        }
        task.resume()
        
    }
   
    
}

